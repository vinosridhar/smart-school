# **Android Task - Video** #
* An NGO is helping village kids by giving them Android phones so they can watch educational videos. 
* The NGO wants an Android application on which these kids can watch videos. However, the village kids only have internet access in the NGO center. The NGO wants an Android application that saves the video while a kid is watching it. 
* This means that the kid will not need to re-download that video to watch it 

**Challenge:**
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                   
* A video should be saved in the internal storage while user is streaming it.
* You can use this link to access the video: ﻿https://socialcops.com/video/main.mp4﻿
* Make sure there is a single request going to the server for streaming and saving on the Android phone.

![device-2016-12-16-135103.png](https://bitbucket.org/repo/gddG8r/images/175398076-device-2016-12-16-135103.png)
![device-2016-12-16-135207.png](https://bitbucket.org/repo/gddG8r/images/1892957268-device-2016-12-16-135207.png)