package in.sri.smartschool.presenters;

import in.sri.smartschool.views.HomeView;

/**
 * Created by sridhar on 10/12/16.
 */

public class HomePresenter implements BasePresenter<HomeView> {

    private HomeView view;

    @Override
    public void attachView(HomeView view) {
        this.view = view;
    }

    @Override
    public void detachView() {
        view = null;
    }
}
