package in.sri.smartschool.presenters;

import android.os.Handler;
import android.os.Looper;
import android.view.SurfaceView;
import android.widget.Toast;

import in.sri.smartschool.models.Video;
import in.sri.smartschool.player.VideoPlayer;
import in.sri.smartschool.utils.DownloadTask;
import in.sri.smartschool.utils.L;
import in.sri.smartschool.utils.WorkerThread;
import in.sri.smartschool.views.VideoBaseView;


/**
 * Created by sridhar on 12/12/16.
 */

public class VideoPresenter implements BasePresenter<VideoBaseView> {

    private static final String TAG = VideoPresenter.class.getSimpleName();

    private VideoBaseView view;
    private WorkerThread workerThread;
    private VideoPlayer videoPlayer;
    private Handler uiHander = new Handler(Looper.getMainLooper());
    private DownloadTask downloadTask;
    public int currentDownloadStatus = DownloadTask.NONE;
    public int currentPlayerStatus = VideoPlayer.STATUS_NONE;

    private Runnable hideControlsRunnable = new Runnable() {
        @Override
        public void run() {
            view.showControls(false);
        }
    };

    private Runnable durationChangeRunnable = new Runnable() {
        @Override
        public void run() {
            view.updateDuration();
            uiHander.removeCallbacks(durationChangeRunnable);
            uiHander.postDelayed(durationChangeRunnable, 500);
        }
    };

    private Runnable seekbarChangeRunnable = new Runnable() {
        @Override
        public void run() {
            view.updateSeekbar();
            uiHander.removeCallbacks(seekbarChangeRunnable);
            uiHander.postDelayed(seekbarChangeRunnable, 100);
        }
    };

    @Override
    public void attachView(VideoBaseView view) {
        this.view = view;
        workerThread = new WorkerThread(TAG);
        workerThread.start();
        workerThread.prepare();
    }

    @Override
    public void detachView() {
        try {
            if (workerThread != null) {
                workerThread.quit();
                workerThread = null;
            }
            if (videoPlayer != null) {
                videoPlayer.cancel();
                videoPlayer.pauseVideo();
                videoPlayer.release();
                uiHander.removeCallbacksAndMessages(null);
            }
            if (downloadTask != null) {
                if (!downloadTask.isCompleted()) {
                    downloadTask.cancel();
                }
            }
        } catch (Exception e) {
            L.logException(TAG, e);
        } finally {
            view = null;
        }
    }

    public void playVideo(Video video, SurfaceView videoView) {
        videoPlayer = new VideoPlayer(videoView, new VideoPlayer.VideoPlayerHandler() {
            @Override
            public void statusChanged() {
                currentPlayerStatus = videoPlayer.getStatus();
                switch (videoPlayer.getStatus()) {
                    case VideoPlayer.STATUS_PLAYING:
                        view.playing();
                        startVideoPlayerCallbacks();
                        break;
                    case VideoPlayer.STATUS_BUFFERING:
                        view.buffering();
                        break;
                    case VideoPlayer.STATUS_PAUSED:
                        view.paused();
                        stopVideoPlayerCallbacks();
                        break;
                    case VideoPlayer.STATUS_PREPARED:
                        prepared();
                        break;
                    case VideoPlayer.STATUS_COMPLETED:
                        completed();
                        break;
                    case VideoPlayer.STATUS_PREPARING:
                        view.showProgress(true);
                        break;
                    case VideoPlayer.STATUS_ERROR:
                        view.error();
                        break;
                }
            }
        });
        videoPlayer.start(video);
    }

    public void downloadVideo(final Video video) {
        Toast.makeText(view.getContext(), "Download started", Toast.LENGTH_LONG).show();
        downloadTask = new DownloadTask(video.getUrl(), video.getFileName(), new DownloadTask.DownloadTaskHandler() {
            @Override
            public void stateChanged() {
                uiHander.post(new Runnable() {
                    @Override
                    public void run() {
                        currentDownloadStatus = downloadTask.getStatus();
                        switch (downloadTask.getStatus()) {
                            case DownloadTask.COMPLETE:
                                view.completeDownload();
                                Toast.makeText(view.getContext(), "Download completed", Toast.LENGTH_LONG).show();
                                break;
                            case DownloadTask.DOWNLOADING:
                                String downloadProgress = downloadTask.getDownloadedSize() + " of " + downloadTask.getFileSize();
                                view.updateDownloadProgress(downloadProgress);
                                view.playDownload();
                                break;
                            case DownloadTask.PAUSE:
                                view.pauseDownload();
                                break;
                        }
                    }
                });
            }
        });
        downloadTask.download();
        startDownload();
    }

    public synchronized void startDownload() {
        workerThread.doWork(downloadTask);
    }

    public synchronized VideoPlayer getVideoPlayer() {
        return videoPlayer;
    }

    public synchronized DownloadTask getDownloadTask() {
        return downloadTask;
    }

    private void startVideoPlayerCallbacks() {
        stopVideoPlayerCallbacks();
        uiHander.postDelayed(durationChangeRunnable, 500);
        uiHander.postDelayed(seekbarChangeRunnable, 100);
        uiHander.postDelayed(hideControlsRunnable, 5000);
    }

    private void stopVideoPlayerCallbacks() {
        uiHander.removeCallbacks(durationChangeRunnable);
        uiHander.removeCallbacks(seekbarChangeRunnable);
        uiHander.removeCallbacks(hideControlsRunnable);
    }

    private void prepared() {
        view.playerPrepared();
        startVideoPlayerCallbacks();
    }

    private void completed() {
        stopVideoPlayerCallbacks();
        videoPlayer.prepareForReplay();
        view.completed();
    }
}
