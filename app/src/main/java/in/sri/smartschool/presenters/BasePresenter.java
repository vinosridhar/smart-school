package in.sri.smartschool.presenters;

/**
 * Created by sridhar on 10/12/16.
 */

public interface BasePresenter<T> {

    void attachView(T view);
    void detachView();

}
