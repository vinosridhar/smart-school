package in.sri.smartschool.utils;

import android.view.View;

/**
 * Created by sridhar on 12/12/16.
 */

public class Listeners {

    public interface RecyclerViewOnClickListener {
        void onClick(View v, int position);
    }

}
