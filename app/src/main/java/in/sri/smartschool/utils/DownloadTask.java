package in.sri.smartschool.utils;

import android.support.annotation.NonNull;

import java.io.File;
import java.io.InputStream;
import java.io.RandomAccessFile;
import java.net.HttpURLConnection;
import java.net.URL;

import in.sri.smartschool.SmartSchoolApp;

/**
 * Created by sridhar on 12/12/16.
 */

public class DownloadTask implements Runnable {

    private static final String TAG = DownloadTask.class.getSimpleName();

    public interface DownloadTaskHandler {
        void stateChanged();
    }

    public static final int BUFFER_SIZE = 1 * 1024; //1kb

    //task status
    public static final int DOWNLOADING = 1;
    public static final int PAUSE = 2;
    public static final int ERROR = 3;
    public static final int COMPLETE = 4;
    public static final int CANCEL = 5;
    public static final int NONE = 6;

    private int status = NONE;
    private URL url;
    public int downloadedSize = 0;
    public int downloadFileSize = -1;
    private String lastModified;
    private DownloadTaskHandler handler;
    private String fileName;

    public DownloadTask(@NonNull URL url, String fileName, DownloadTaskHandler handler) {
        this.url = url;
        this.fileName = fileName;
        this.handler = handler;
    }

    public synchronized int getStatus() {
        return status;
    }

    public boolean isCompleted() {
        return status == COMPLETE;
    }

    public boolean isDownloading() {
        return status == DOWNLOADING;
    }

    public boolean isPaused() {
        return status == PAUSE;
    }

    public String getDownloadFileName() {
        if (fileName == null) {
            L.d(TAG, "file name is null");
            String fileName = url.getFile();
            this.fileName = fileName.substring(fileName.lastIndexOf("/") + 1);
        }
        return fileName;
    }

    public String getDownloadFilePath() {
        return SmartSchoolApp.getDownloadsDirectory() + getDownloadFileName();
    }

    public File getDownloadFile() {
        return new File(getDownloadFilePath());
    }

    public float getProgress() {
        return (float) (downloadedSize / downloadFileSize) * 100;
    }

    public void complete() {
        status = COMPLETE;
        stateChanged();
    }

    public void error() {
        status = ERROR;
        stateChanged();
    }

    public void pause() {
        status = PAUSE;
        stateChanged();
    }

    public void resume() {
        download();
        stateChanged();
    }

    public void cancel() {
        L.d(TAG, "canceling download task");
        //delete the file
        File file = getDownloadFile();
        file.delete();
        status = CANCEL;
        stateChanged();
    }

    public void download() {
        status = DOWNLOADING;
    }

    public String getFileSize() {
        return bytesToKb(downloadFileSize);
    }

    public String getDownloadedSize() {
        return bytesToKb(downloadedSize);
    }

    private void stateChanged() {
//        L.d(TAG, "Downloadtask status: " + status);
        if (handler != null) {
            synchronized (handler) {
                handler.stateChanged();
            }
        }
    }

    private String bytesToKb(int bytes) {
        return bytes/1024 + "kb";
    }

    private void log() {
        L.d(TAG, "downloading file: " + url.toString());
        L.d(TAG, "file size: " + bytesToKb(downloadFileSize));
        L.d(TAG, "downloaded: " + bytesToKb(downloadedSize));
        L.d(TAG, "yet to download size: " + bytesToKb(downloadFileSize - downloadedSize));
        L.d(TAG, "status: " + getStatus());
    }

    @Override
    public void run() {
        if (url == null) {
            L.e(TAG, "url is null");
            error();
            return;
        }

        InputStream is = null;
        RandomAccessFile file = null;

        try {
            HttpURLConnection urlConnection = (HttpURLConnection) url.openConnection();
            //for resuming download purpose
            urlConnection.setRequestProperty("Range", "bytes=" + downloadedSize + "-");
            if (lastModified != null) {
                urlConnection.setRequestProperty("If-Range", lastModified);
            }
            urlConnection.connect();

            int responseCode = urlConnection.getResponseCode();
            L.d(TAG, "response code: " + responseCode);
            if (responseCode/100 != 2) {
                L.d(TAG, "response code is not success");
                error();
            }

            int contentLength = urlConnection.getContentLength();
            if (contentLength < 1) {
                error();
            }

            //starting download for the first time
            if (downloadFileSize == -1) {
                downloadFileSize = contentLength;
                lastModified = urlConnection.getHeaderField("Last-Modified");
                stateChanged();
            }

            file = new RandomAccessFile(getDownloadFilePath(), "rw");
            file.seek(downloadedSize);

            is = urlConnection.getInputStream();

            while (status == DOWNLOADING) {
                int byteToBeDownloaded = downloadFileSize - downloadedSize;
                byte[] buffer = new byte[byteToBeDownloaded < BUFFER_SIZE ? byteToBeDownloaded : BUFFER_SIZE];
                int read = is.read(buffer);
                if (read == -1) {
                    break;
                }

                file.write(buffer, 0, read);
                downloadedSize += read;
//                log();
                stateChanged();
//                Thread.sleep(1000);
            }

            if (status == DOWNLOADING) {
                //to prevent calling multiple times sleep for sometime
                Thread.sleep(100);
                complete();
            }

        } catch (Exception e) {
            L.logException(TAG, e);
            error();
        } finally {
            if (is != null) {
                try {
                    is.close();
                } catch (Exception e) {
                    L.logException(TAG, e);
                }
            }
            if (file != null) {
                try {
                    file.close();
                } catch (Exception e) {
                    L.logException(TAG, e);
                }
            }
        }
    }
}
