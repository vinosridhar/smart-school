package in.sri.smartschool.utils;

import android.graphics.Bitmap;
import android.media.ThumbnailUtils;
import android.provider.MediaStore;

import java.io.File;

import in.sri.smartschool.SmartSchoolApp;

/**
 * Created by sridhar on 14/12/16.
 */

public class VideoUtils {

    private static final String TAG = VideoUtils.class.getSimpleName();

    public static boolean createThumbnail(String thumbnailPath, String videoPath) {
        try {
            Bitmap bitmap = ThumbnailUtils.createVideoThumbnail(videoPath, MediaStore.Video.Thumbnails.MINI_KIND);
            FileUtils.saveBitmap(bitmap, thumbnailPath);
            return true;
        } catch (Exception e) {
            L.logException(TAG, e);
            return false;
        }
    }

    public static boolean hasThumbnail(String thumbnailName) {
        String thumbnailPath = SmartSchoolApp.getThumbnailsDirectory() + thumbnailName;
        return new File(thumbnailPath).exists();
    }

}
