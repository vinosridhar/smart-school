package in.sri.smartschool.utils;

import android.app.Activity;
import android.content.Context;
import android.graphics.Typeface;
import android.support.v4.util.LruCache;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnTouchListener;
import android.view.inputmethod.InputMethodManager;
import android.widget.Toast;


public class ViewUtils {

	public final static String FONTAWESOME = "FONTAWESOME";
	private static final LruCache<String, Typeface> sTypefaceCache = new LruCache<String, Typeface>(12);
	
	public static void toast(Context c, String message) {
		Toast.makeText(c.getApplicationContext(), message, Toast.LENGTH_LONG).show();
	}

	public static void hideSoftKeyboard(final Activity activity, View v) {
		v.setOnTouchListener(new OnTouchListener() {
			@Override
			public boolean onTouch(View v, MotionEvent event) {
				switch (event.getAction()) {
					case MotionEvent.ACTION_DOWN:
						hideSoftKeyboard(activity);
						break;
					default:
						break;
				}
				return false;
			}
		});
	}
	
	public static void hideSoftKeyboard(Activity activity) {
		try {
			InputMethodManager inputMethodManager = (InputMethodManager) activity.getSystemService(Activity.INPUT_METHOD_SERVICE);
			inputMethodManager.hideSoftInputFromWindow(activity.getCurrentFocus().getWindowToken(), 0);
			inputMethodManager.hideSoftInputFromWindow(activity.getCurrentFocus().getApplicationWindowToken(), 0);
		} catch(Exception ex) {
			//Device with physical keyboard
		}
	}

	public static Typeface getFont(Context c, String name) {
		Typeface typeface = sTypefaceCache.get(name);
		if (typeface == null) {
			typeface = Typeface.createFromAsset(c.getApplicationContext().getAssets(), "fontawesome-webfont.ttf");
			sTypefaceCache.put(name, typeface);
		}
		return sTypefaceCache.get(name);
	}
}
