package in.sri.smartschool.utils;

import android.os.Handler;
import android.os.HandlerThread;

/**
 * Created by sridhar on 12/12/16.
 */

public class WorkerThread extends HandlerThread {

    private Handler handler;

    public WorkerThread(String name) {
        super(name);
    }

    public void doWork(Runnable work) {
        handler.post(work);
    }

    public void prepare() {
        handler = new Handler(getLooper());
    }
}
