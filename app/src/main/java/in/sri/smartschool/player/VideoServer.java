package in.sri.smartschool.player;

import java.io.ByteArrayInputStream;
import java.io.RandomAccessFile;
import java.util.Map;

import in.sri.smartschool.models.Video;
import in.sri.smartschool.utils.L;
import in.sri.smartschool.utils.NanoHTTPD;

public class VideoServer extends NanoHTTPD {

    private static final String TAG = VideoServer.class.getSimpleName();

    public static final int PORT = 8080;
    public static final int BUFFER_SIZE = 100000 * 1024; //1kb

    //headers
    public static final String RANGE = "range";

    private Video video;

    public VideoServer(Video video) {
        super(PORT);
        this.video = video;
    }

    @Override
    public Response serve(final IHTTPSession session) {
        L.d(TAG, "request method: " + session.getMethod());

        try {
            RandomAccessFile file = new RandomAccessFile(video.getLocalFilePath(), "rw");
            if (file.length() == video.getVideoSize()) {
                byte[] buffer = new byte[(int) file.length()];
                file.read(buffer);
                return sendResponse(0, buffer);
            }
        } catch (Exception e) {
            L.logException(TAG, e);
        }

        Map<String, String> headers = session.getHeaders();

        L.d(TAG, "headers: " + headers.toString());

        long range = 0;

        //range=bytes=1942356-
        if (headers.containsKey(RANGE)) {
            String rangeString = headers.get(RANGE);
            L.d(TAG, "range string: " + rangeString);
            range = Integer.parseInt(rangeString.substring(rangeString.indexOf("=") + 1, rangeString.indexOf("-")));
        }

        //check for buffering state

        //

        try {
            final long bufferingSize = range == 0 ? video.getVideoSize() : range;
            final RandomAccessFile randomAccessFile = new RandomAccessFile(video.getLocalFilePath(), "rw");
            if (bufferingSize > randomAccessFile.length()) {
                L.d(TAG, "enter into buffering state");
                RandomAccessFile tempRandomAccessFile = null;
                while (true) {
                    try {
                        Thread.sleep(1000);
                        L.d(TAG, "buffering");
                        tempRandomAccessFile = new RandomAccessFile(video.getLocalFilePath(), "rw");
                        if (video.getVideoSize() == tempRandomAccessFile.length() || tempRandomAccessFile.length() > bufferingSize) {
                            L.d(TAG, "buffering end");
                            break;
                        }
                    } catch (Exception e) {
                        L.logException(TAG, e);
                    }
                }
            }
        } catch (Exception e) {
            L.logException(TAG, e);
        }

        //start serving

        byte[] buffer = null;
        RandomAccessFile randomAccessFile = null;

        try {
            randomAccessFile = new RandomAccessFile(video.getLocalFilePath(), "rw");

            //check video size and file size or equal
            final long fileSize = randomAccessFile.length();
            long actualVideoSize = video.getVideoSize();

            L.d(TAG, String.format("file size %s | actual video size %s", fileSize, actualVideoSize));
            L.d(TAG, String.format("requested range %s", range));

            if (fileSize != actualVideoSize) {
                //being download actual video
                buffer = new byte[(int)fileSize];
            } else {
                int remainingSize = (int) (fileSize - range);
                if (remainingSize < BUFFER_SIZE) {
                    buffer = new byte[remainingSize];
                } else {
                    buffer = new byte[BUFFER_SIZE];
                }
            }

            if (range != 0) {
                randomAccessFile.seek(range);
            }

            randomAccessFile.read(buffer, (int) range, buffer.length);

        } catch (Exception e) {
            L.logException(TAG, e);
        } finally {
            if (randomAccessFile != null) {
                try {
                    randomAccessFile.close();
                } catch (Exception e) {
                    L.logException(TAG, e);
                }
            }
        }

        return sendResponse(range, buffer);
    }

    private Response sendResponse(long range, byte[] buffer) {
        Response response = NanoHTTPD.newFixedLengthResponse(Response.Status.PARTIAL_CONTENT,"video/mp4", new ByteArrayInputStream(buffer), video.getVideoSize());

        response.setGzipEncoding(true);

//        response.setKeepAlive(true);

        response.addHeader("Accept-Ranges", "bytes");

        response.addHeader("Content-Length", String.valueOf(video.getVideoSize()));

        response.addHeader("Content-Range", String.format("bytes %s-%s/%s", range, (range + buffer.length), buffer.length));

        L.d(TAG, String.format("Sending %s from %s", video.getLocalFilePath(), getListeningPort()));
        L.d(TAG, String.format("Requested range %s | buffer size %s | range %s", range, buffer.length, range + buffer.length));

        return response;
    }
}