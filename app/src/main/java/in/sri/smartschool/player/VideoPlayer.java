package in.sri.smartschool.player;

import android.app.Activity;
import android.content.Context;
import android.graphics.Point;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.view.SurfaceHolder;
import android.view.SurfaceView;

import in.sri.smartschool.models.Video;
import in.sri.smartschool.utils.L;


/**
 * Created by sridhar on 13/12/16.
 */

public class VideoPlayer extends MediaPlayer implements MediaPlayer.OnCompletionListener, MediaPlayer.OnInfoListener, MediaPlayer.OnPreparedListener, MediaPlayer.OnErrorListener, MediaPlayer.OnSeekCompleteListener {

    public static final int STATUS_NONE = 0;
    public static final int STATUS_PLAYING = 1;
    public static final int STATUS_BUFFERING = 2;
    public static final int STATUS_COMPLETED = 3;
    public static final int STATUS_PAUSED = 4;
    public static final int STATUS_PREPARED = 5;
    public static final int STATUS_ERROR = 6;
    public static final int STATUS_PREPARING = 7;
    private static final String TAG = VideoPlayer.class.getSimpleName();
    private SurfaceView videoView;
    private int status = STATUS_NONE;
    private VideoPlayerHandler videoPlayerHandler;
    private boolean prepared = false;
    private boolean cancel = false;
    private int positionWhenPrepared;
    private VideoServer videoServer;

    public VideoPlayer(SurfaceView videoView, VideoPlayerHandler handler) {
        this.videoView = videoView;
        this.videoPlayerHandler = handler;
    }

    public int getStatus() {
        return status;
    }

    public void play() {
        if (!isPrepared() || isPlaying()) {
            return;
        }
        start();
        status = STATUS_PLAYING;
        statusChanged();
    }

    public void replay() {
        play();
    }

    public void prepareForReplay() {
        if (!isPrepared() || isPlaying()) {
            return;
        }
        pause();
        seekTo(0);
    }

    public void start(final Video video) {
        try {
            new Thread(new Runnable() {
                @Override
                public void run() {
                    try {
                        setDisplay(videoView.getHolder());
                        videoServer = new VideoServer(video);
                        videoServer.start();
                        setDataSource("http://127.0.0.1:" + videoServer.getListeningPort());
                        setAudioStreamType(AudioManager.STREAM_MUSIC);
                        prepared = false;
                        setOnPreparedListener(VideoPlayer.this);
                        setOnErrorListener(VideoPlayer.this);
                        prepareAsync();
                        status = STATUS_PREPARING;
                        statusChanged();
                    } catch (Exception e) {
                        L.logException(TAG, e);
                        error();
                    }
                }
            }).start();
        } catch (Exception e) {
            L.logException(TAG, e);
            error();
        }
    }

    public void setHolder(SurfaceHolder holder) {
        setDisplay(holder);
    }

    public void pauseVideo() {
        if (!isPrepared()) {
            return;
        }
        pause();
        status = STATUS_PAUSED;
        statusChanged();
    }

    public void resumeVideo() {
        if (!isPrepared()) {
            return;
        }
        start();
        status = STATUS_PLAYING;
        statusChanged();
    }

    public String getDurationReadable() {
        if (!isPrepared()) {
            return "";
        }
        return milliToMinute(getDuration());
    }

    public String getCurrentPositionReadable() {
        if (!isPrepared()) {
            return "";
        }
        return milliToMinute(getCurrentPosition());
    }

    public int getVideoDuration() {
        if (!isPrepared()) {
            return 0;
        }
        return getDuration();
    }

    public int getVideoCurrentDuration() {
        if (!isPrepared()) {
            return 0;
        }
        return getCurrentPosition();
    }

    public SurfaceView getVideoView() {
        return videoView;
    }

    public boolean isPrepared() {
        return prepared;
    }

    public void setPositionWhenPrepared(int position) {
        this.positionWhenPrepared = position;
    }

    public void cancel() {
        if (videoServer != null && videoServer.isAlive()) {
            videoServer.stop();
        }
        this.cancel = true;
    }

    private String milliToMinute(int milli) {
        int t = milli / 1000;
        int min = t / 60;
        int sec = t % 60;
        return String.format("%02d:%02d", min, sec);
    }

    private void statusChanged() {
        L.d(TAG, "status changed: " + status);
        if (videoPlayerHandler != null) {
            videoPlayerHandler.statusChanged();
        }
    }

    private void error() {
        status = STATUS_ERROR;
        statusChanged();
    }

    public void setVideoSize() {
        // // Get the dimensions of the video
        int videoWidth = getVideoWidth();
        int videoHeight = getVideoHeight();
        float videoProportion = (float) videoWidth / (float) videoHeight;

        // Get the width of the screen
        Context context = videoView.getContext();
        Point point = new Point();
        ((Activity) context).getWindowManager().getDefaultDisplay().getSize(point);
        int screenWidth = point.x;
        int screenHeight = point.y;
        float screenProportion = (float) screenWidth / (float) screenHeight;

        // Get the SurfaceView layout parameters
        android.view.ViewGroup.LayoutParams lp = videoView.getLayoutParams();
        if (videoProportion > screenProportion) {
            lp.width = screenWidth;
            lp.height = (int) ((float) screenWidth / videoProportion);
        } else {
            lp.width = (int) (videoProportion * (float) screenHeight);
            lp.height = screenHeight;
        }
        // Commit the layout parameters
        videoView.setLayoutParams(lp);
    }

    @Override
    public void onCompletion(MediaPlayer mp) {
        L.d(TAG, "onCompletion()");
        status = STATUS_COMPLETED;
        statusChanged();
    }

    @Override
    public boolean onInfo(MediaPlayer mp, int what, int extra) {
        switch (what) {
            case MediaPlayer.MEDIA_INFO_BUFFERING_START:
                status = STATUS_BUFFERING;
                statusChanged();
                break;
            case MediaPlayer.MEDIA_INFO_BUFFERING_END:
                if (mp.isPlaying()) {
                    status = STATUS_PLAYING;
                } else {
                    status = STATUS_PAUSED;
                }
                statusChanged();
                break;
        }
        return false;
    }

    @Override
    public void onPrepared(MediaPlayer mp) {
        L.d(TAG, "onPrepared()");
        if (cancel) {
            mp.release();
            return;
        }
        prepared = true;
        setOnInfoListener(this);
        setOnCompletionListener(this);
        setVideoSize();
        status = STATUS_PREPARED;
        statusChanged();
        if (positionWhenPrepared != 0) {
            seekTo(positionWhenPrepared);
        }
    }

    @Override
    public boolean onError(MediaPlayer mp, int what, int extra) {
        L.d(TAG, "onError()");
        error();
        return true;
    }

    @Override
    public void onSeekComplete(MediaPlayer mp) {
        L.d(TAG, "onSeekComplete()");
    }

    public interface VideoPlayerHandler {
        void statusChanged();
    }
}
