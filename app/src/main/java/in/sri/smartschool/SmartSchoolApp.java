package in.sri.smartschool;

import android.Manifest;
import android.app.Application;

import java.io.File;

import in.sri.smartschool.utils.FileUtils;
import in.sri.smartschool.utils.PermissionUtils;
import rx.Scheduler;
import rx.schedulers.Schedulers;

/**
 * Created by sridhar on 10/12/16.
 */

public class SmartSchoolApp extends Application {

    private static Scheduler defaultScheduler;
    private static SmartSchoolApp instance = null;

    public static final String[] APP_PERMISSIONS = {
            Manifest.permission.WRITE_EXTERNAL_STORAGE,
    };

    @Override
    public void onCreate() {
        super.onCreate();
        instance = this;
        createDownloadsDirectory();
    }

    public static SmartSchoolApp getInstance() {
        return instance;
    }

    public static Scheduler getDefaultScheduler() {
        if (defaultScheduler == null) {
            defaultScheduler = Schedulers.io();
        }
        return defaultScheduler;
    }

    public static String getDownloadsDirectory() {
        return FileUtils.getSDCardPath() + BuildConfig.APP_NAME + File.separator;
    }

    public static String getThumbnailsDirectory() {
        return getDownloadsDirectory() + ".thumbnails" + File.separator;
    }

    public static void createDownloadsDirectory() {
        if (FileUtils.isSDCardEnable() && PermissionUtils.hasPermission(getInstance(), Manifest.permission.WRITE_EXTERNAL_STORAGE)) {
            FileUtils.createOrExistsDir(FileUtils.getFileByPath(getDownloadsDirectory()));
            FileUtils.createOrExistsDir(FileUtils.getFileByPath(getThumbnailsDirectory()));
        }
    }
}
