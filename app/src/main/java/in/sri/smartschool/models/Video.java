package in.sri.smartschool.models;

import android.os.Parcel;
import android.os.Parcelable;

import java.io.File;
import java.net.URL;

import in.sri.smartschool.SmartSchoolApp;

/**
 * Created by sridhar on 12/12/16.
 */

public class Video implements Parcelable {

    private URL url;
    private String title;
    private String fileName;
    private long videoSize;

    public Video(URL url, String title, String fileName) {
        this.url = url;
        this.title = title;
        this.fileName = fileName;
        if (isOffline()) {
            File file = new File(getLocalFilePath());
            setVideoSize(file.length());
        }
    }

    public URL getUrl() {
        return url;
    }

    public String getTitle() {
        return this.title;
    }

    public String getFileName() {
        return fileName;
    }

    public boolean isOffline() {
        return new File(getLocalFilePath()).exists();
    }

    public String getLocalFilePath() {
        String fileAbsolutePath = SmartSchoolApp.getDownloadsDirectory() + getFileName();
        return fileAbsolutePath;
    }

    public void setVideoSize(long size) {
        this.videoSize = size;
    }

    public long getVideoSize() {
        return videoSize;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeSerializable(this.url);
        dest.writeString(this.title);
        dest.writeString(this.fileName);
        dest.writeLong(this.videoSize);
    }

    protected Video(Parcel in) {
        this.url = (URL) in.readSerializable();
        this.title = in.readString();
        this.fileName = in.readString();
        this.videoSize = in.readLong();
    }

    public static final Creator<Video> CREATOR = new Creator<Video>() {
        @Override
        public Video createFromParcel(Parcel source) {
            return new Video(source);
        }

        @Override
        public Video[] newArray(int size) {
            return new Video[size];
        }
    };
}
