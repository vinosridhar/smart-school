package in.sri.smartschool.views.activity;

import android.Manifest;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;

import in.sri.smartschool.SmartSchoolApp;
import in.sri.smartschool.utils.L;
import in.sri.smartschool.utils.ViewUtils;

/**
 * Created by sridhar on 10/12/16.
 */

/*
* Abstract activity for all the activities
* */
public class BaseActivity extends AppCompatActivity {

    protected final String TAG = this.getClass().getSimpleName();

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        L.d(TAG, "inside onCreate()");
        super.onCreate(savedInstanceState);
    }

    @Override
    protected void onStop() {
        L.d(TAG, "inside onStop()");
        super.onStop();
    }

    @Override
    protected void onDestroy() {
        L.d(TAG, "inside onDestroy()");
        //we don't want to keep keyboard
        ViewUtils.hideSoftKeyboard(this);
        super.onDestroy();
    }

    @Override
    protected void onPause() {
        L.d(TAG, "inside onPause()");
        super.onPause();
    }

    @Override
    protected void onResume() {
        L.d(TAG, "inside onResume()");
        super.onResume();
    }

    @Override
    public void onBackPressed() {
        L.d(TAG, "inside onBackPressed()");
        super.onBackPressed();
    }

    /*
    * To show back button in the action bar
    * */
    protected void showBack() {
        ActionBar actionBar = getSupportActionBar();
        actionBar.setDisplayHomeAsUpEnabled(true);
        actionBar.setHomeButtonEnabled(false);
        actionBar.setDisplayUseLogoEnabled(false);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        for (int i = 0; i < permissions.length; i++) {
            String permission = permissions[i];
            int result = grantResults[i];
            if (permission.equals(Manifest.permission.WRITE_EXTERNAL_STORAGE)) {
                if (result == PackageManager.PERMISSION_GRANTED) {
                    SmartSchoolApp.createDownloadsDirectory();
                }
            }
        }
    }
}
