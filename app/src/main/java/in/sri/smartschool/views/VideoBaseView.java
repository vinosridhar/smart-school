package in.sri.smartschool.views;

import in.sri.smartschool.player.VideoPlayer;
import in.sri.smartschool.utils.DownloadTask;

/**
 * Created by sridhar on 12/12/16.
 */

public interface VideoBaseView extends BaseView {

    void showProgress(boolean show);
    void showControls(boolean show);
    void updateDuration();
    void updateSeekbar();
    void playerPrepared();
    void playing();
    void buffering();
    void paused();
    void completed();
    void error();
    void startingDownload();
    void updateDownloadProgress(String text);
    void pauseDownload();
    void playDownload();
    void completeDownload();
    VideoPlayer getVideoPlayer();
    DownloadTask getDownloadTask();

}
