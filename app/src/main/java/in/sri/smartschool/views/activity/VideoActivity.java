package in.sri.smartschool.views.activity;

import android.Manifest;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.content.pm.PackageManager;
import android.content.res.Configuration;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.view.View;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.Toast;

import java.io.File;

import butterknife.BindView;
import butterknife.ButterKnife;
import in.sri.smartschool.R;
import in.sri.smartschool.SmartSchoolApp;
import in.sri.smartschool.models.Video;
import in.sri.smartschool.player.VideoPlayer;
import in.sri.smartschool.presenters.VideoPresenter;
import in.sri.smartschool.utils.DownloadTask;
import in.sri.smartschool.utils.L;
import in.sri.smartschool.utils.PermissionUtils;
import in.sri.smartschool.utils.VideoUtils;
import in.sri.smartschool.views.VideoBaseView;

/**
 * Created by sridhar on 12/12/16.
 */

public class VideoActivity extends BaseActivity implements VideoBaseView, SurfaceHolder.Callback {

    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.videoView)
    SurfaceView videoView;
    @BindView(R.id.progressBar)
    ProgressBar progressBar;
    @BindView(R.id.pauseResumeButton)
    ImageView pauseResumeButton;
    @BindView(R.id.controlsContainer)
    LinearLayout controlsContainer;
    @BindView(R.id.currentDurationTextView)
    TextView currentDurationTextView;
    @BindView(R.id.durationTextView)
    TextView durationTextView;
    @BindView(R.id.videoSeekbar)
    SeekBar videoSeekbar;
    @BindView(R.id.orientationChangeButton)
    ImageView orientationChangeButton;
    @BindView(R.id.downloadText)
    TextView downloadText;
    @BindView(R.id.downloadPauseResumeButton)
    ImageView downloadPauseResumeButton;
    @BindView(R.id.title)
    TextView title;

    private VideoPresenter presenter;
    private Video video;
    private boolean controls;
    private int orientation;
    private int currentPosition;
    private AlertDialog errorDialog;

    public static Intent newIntent(Context context) {
        Intent i = new Intent(context, VideoActivity.class);
        return i;
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        //only go further if we have video parcel
        if (getIntent() == null || !getIntent().hasExtra("video") || getIntent().getParcelableExtra("video") == null) {
            finish();
            return;
        }

        video = getIntent().getParcelableExtra("video");

        presenter = new VideoPresenter();
        presenter.attachView(this);

        setContentView(R.layout.activity_video);

        ButterKnife.bind(this);

        orientation = getResources().getConfiguration().orientation;
        currentPosition = 0;
        if (savedInstanceState != null) {
            if (savedInstanceState.containsKey("video_current_position")) {
                currentPosition = savedInstanceState.getInt("video_current_position");
            }
            if (savedInstanceState.containsKey("current_orientation")) {
                orientation = savedInstanceState.getInt("current_orientation");
            }
        }

        initComponents();
    }

    private void initComponents() {
        toolbar.setBackgroundColor(Color.TRANSPARENT);
        toolbar.setTitle(video.getTitle());
        title.setText(video.getTitle());
        setSupportActionBar(toolbar);

        getSupportActionBar().setHomeAsUpIndicator(R.drawable.ic_action_navigation_arrow_back);
        getSupportActionBar().setBackgroundDrawable(new ColorDrawable(getResources().getColor(R.color.player_control_bg)));

        showBack();

        updateOrientation();

        videoView.getHolder().addCallback(this);

        prepareVideoForDownload();
    }

    /*
    * Video Player UI Handles
    * */

    public void prepareVideoForPlay() {
        //screen touch listener
        findViewById(android.R.id.content).setOnTouchListener(screenTouchListener);
        controlsContainer.setOnTouchListener(videoControlsTouchListener);
        videoSeekbar.setOnSeekBarChangeListener(videoSeekBarChangeListener);
        presenter.playVideo(video, videoView);
        getVideoPlayer().setPositionWhenPrepared(currentPosition);
    }

    @Override
    public void showProgress(boolean show) {
        progressBar.setVisibility(show ? View.VISIBLE : View.GONE);
    }

    public void showControls(boolean show) {
        controls = show;
        if (show) {
            getSupportActionBar().show();
            if (getVideoPlayer().isPrepared()) {
                controlsContainer.setVisibility(View.VISIBLE);
            }
        } else {
            getSupportActionBar().hide();
            controlsContainer.setVisibility(View.GONE);
        }
    }

    public void updateDuration() {
        currentDurationTextView.setText(getVideoPlayer().getCurrentPositionReadable());
    }

    public void updateSeekbar() {
        videoSeekbar.setProgress(getVideoPlayer().getVideoCurrentDuration());
    }

    @Override
    public void playerPrepared() {
        durationTextView.setText(getVideoPlayer().getDurationReadable());
        videoSeekbar.setMax(getVideoPlayer().getVideoDuration());
        showProgress(false);
        showControls(true);
        updateDuration();
        updateSeekbar();
        getVideoPlayer().play();
    }

    @Override
    public void playing() {
        showProgress(false);
        pauseResumeButton.setImageResource(R.drawable.ic_action_av_pause);
    }

    @Override
    public void buffering() {
        showProgress(true);
    }

    @Override
    public void paused() {
        pauseResumeButton.setImageResource(R.drawable.ic_action_av_play_arrow);
        showProgress(false);
    }

    @Override
    public void completed() {
        pauseResumeButton.setImageResource(R.drawable.ic_action_av_replay);
        showControls(true);
        updateDuration();
        updateSeekbar();
    }

    @Override
    public void error() {
        showErrorDialog();
    }

    @Override
    public VideoPlayer getVideoPlayer() {
        return presenter.getVideoPlayer();
    }

    public void showErrorDialog() {
        if (errorDialog != null && errorDialog.isShowing()) {
            return;
        }
        if (!isFinishing()) {
            errorDialog = new AlertDialog.Builder(this)
                    .setTitle("Error")
                    .setMessage("Can't play this video")
                    .setCancelable(false)
                    .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            //if it offline video just delete
                            if (video.isOffline()) {
                                File file = new File(video.getLocalFilePath());
                                file.delete();
                            }
                            dialog.dismiss();
                            finish();
                        }
                    }).show();
        }
    }

    private SeekBar.OnSeekBarChangeListener videoSeekBarChangeListener = new SeekBar.OnSeekBarChangeListener() {
        @Override
        public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
            if (fromUser) {
                getVideoPlayer().seekTo(progress);
                updateDuration();
            }
        }

        @Override
        public void onStartTrackingTouch(SeekBar seekBar) {
            getVideoPlayer().pauseVideo();
        }

        @Override
        public void onStopTrackingTouch(SeekBar seekBar) {
            getVideoPlayer().play();
        }
    };

    /*
    * This will prevent controller view being hide when it touches
    * */
    private View.OnTouchListener videoControlsTouchListener = new View.OnTouchListener() {
        @Override
        public boolean onTouch(View v, MotionEvent event) {
            return true;
        }
    };

    private View.OnTouchListener screenTouchListener = new View.OnTouchListener() {
        @Override
        public boolean onTouch(View v, MotionEvent event) {
            switch (event.getAction()) {
                case MotionEvent.ACTION_DOWN:
                    showControls(!controls);
                    break;
            }
            return false;
        }
    };

    public void togglePlayButtonClick(View v) {
        switch (getVideoPlayer().getStatus()) {
            case VideoPlayer.STATUS_PLAYING:
                getVideoPlayer().pauseVideo();
                break;
            case VideoPlayer.STATUS_PAUSED:
                getVideoPlayer().resumeVideo();
                break;
            case VideoPlayer.STATUS_COMPLETED:
                getVideoPlayer().replay();
                break;
        }
    }

    private void triggerSizeChange() {
        if (getVideoPlayer() != null && getVideoPlayer().isPrepared()) {
            getVideoPlayer().setVideoSize();
        }
    }

    /*
    * End of Player UI Handles
    * */

    /*
    * Download UI Handles
    * */

    private void prepareVideoForDownload() {
        videoView.requestFocus();
        updateDownloadProgress("Download");
        downloadPauseResumeButton.setImageResource(R.drawable.ic_action_file_file_download);

        if (PermissionUtils.hasPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE)) {
            startDownload();
        }
    }

    private void startDownload() {
        if (!video.isOffline()) {
            updateDownloadProgress("Downloading...");
            presenter.downloadVideo(video);
        } else {
            completeDownload();
        }
    }

    @Override
    public void startingDownload() {
        updateDownloadProgress("Downloading...");
    }

    @Override
    public void updateDownloadProgress(String text) {
        downloadText.setText(text);
    }

    @Override
    public void pauseDownload() {
        downloadPauseResumeButton.setImageResource(R.drawable.ic_action_av_play_arrow);
        getDownloadTask().pause();
    }

    @Override
    public void playDownload() {
        downloadPauseResumeButton.setImageResource(R.drawable.ic_action_av_pause);
        if (getVideoPlayer() == null) {
            video.setVideoSize(getDownloadTask().downloadFileSize);
            prepareVideoForPlay();
        }
    }

    @Override
    public void completeDownload() {
        updateDownloadProgress("Downloaded");
        downloadPauseResumeButton.setImageResource(R.drawable.ic_action_file_file_download);
        String thumbnailPath = SmartSchoolApp.getThumbnailsDirectory() + video.getFileName() + ".png";
        if (!new File(thumbnailPath).exists()) {
            VideoUtils.createThumbnail(thumbnailPath, video.getLocalFilePath());
        }
    }

    private void cancelDownload() {
        boolean resumeOnBack = presenter.currentDownloadStatus == DownloadTask.DOWNLOADING;
        if (resumeOnBack) {
            getDownloadTask().pause();
        }
        showCancelDownloadDialog(resumeOnBack);
    }

    private void resumeDownload() {
        getDownloadTask().resume();
        presenter.startDownload();
    }

    @Override
    public DownloadTask getDownloadTask() {
        return presenter.getDownloadTask();
    }

    public void removeOffline() {
        if (getVideoPlayer() != null) {
            getVideoPlayer().pauseVideo();
        }
        new AlertDialog.Builder(this)
                .setTitle("Remove Offline")
                .setMessage("Are you sure you want to remove this video from your device?")
                .setCancelable(false)
                .setPositiveButton("Remove", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        File file = new File(video.getLocalFilePath());
                        if (file.exists()) {
                            L.d(TAG, "file deleted: " + file.delete());
                        }
                        finish();
                        Toast.makeText(VideoActivity.this, "Video removed successfully", Toast.LENGTH_LONG).show();
                    }
                })
                .setNegativeButton("Back", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        getVideoPlayer().resumeVideo();
                        dialog.dismiss();
                    }
                }).show();
    }

    public void downloadClick(View v) {
        boolean noDownload = presenter.currentDownloadStatus == DownloadTask.NONE || presenter.currentDownloadStatus == DownloadTask.COMPLETE;
        if (video.isOffline() && noDownload) {
            removeOffline();
            return;
        }

        int status = getDownloadTask() == null ? DownloadTask.NONE : getDownloadTask().getStatus();

        switch (status) {
            case DownloadTask.NONE:
                if (!PermissionUtils.hasPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE)) {
                    if (getVideoPlayer() != null && getVideoPlayer().isPrepared()) {
                        getVideoPlayer().pauseVideo();
                    }
                    requestPermissions(new String[] {Manifest.permission.WRITE_EXTERNAL_STORAGE}, 1);
                } else {
                    //something else happened here
                }
                break;
            case DownloadTask.DOWNLOADING:
                pauseDownload();
                break;
            case DownloadTask.PAUSE:
                resumeDownload();
                break;
            case DownloadTask.COMPLETE:
                removeOffline();
                break;
        }
    }

    private void showCancelDownloadDialog(final boolean resume) {
        new AlertDialog.Builder(this)
                .setTitle("Cancel Download")
                .setMessage("Download in progress, Do you want to cancel this?")
                .setCancelable(false)
                .setPositiveButton("No", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        if (resume) {
                            resumeDownload();
                        }
                    }
                })
                .setNegativeButton("Yes", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        getDownloadTask().cancel();
                        finish();
                    }
                })
                .show();
    }

    private boolean checkDownloadTask() {
        if (getDownloadTask() != null && !getDownloadTask().isCompleted()) {
            L.d(TAG, "download task is running");
            cancelDownload();
            return true;
        }

        return false;
    }

    /*
    * End of Download UI Handles
    * */

    /*
    * System UI Handles
    * */

    @Override
    public Context getContext() {
        return this;
    }

    @Override
    public void back() {
        if (!checkDownloadTask()) {
            finish();
        }
    }

    @Override
    public void next() {

    }

    @Override
    public void onBackPressed() {
        if (!checkDownloadTask()) {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                back();
                break;
            default:
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onDestroy() {
        if (presenter != null) {
            presenter.detachView();
        }
        super.onDestroy();
    }

    public void updateOrientation() {
        switch (orientation) {
            case ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE:
                getWindow().addFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN);
                orientationChangeButton.setImageResource(R.drawable.ic_action_navigation_fullscreen_exit);
                triggerSizeChange();
                break;
            case ActivityInfo.SCREEN_ORIENTATION_PORTRAIT:
                getWindow().clearFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN);
                orientationChangeButton.setImageResource(R.drawable.ic_action_navigation_fullscreen);
                triggerSizeChange();
                break;
        }
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        L.d(TAG, "orientation changed");
        updateOrientation();
        super.onConfigurationChanged(newConfig);
    }

    @Override
    protected void onPause() {
        if (getVideoPlayer() != null) {
            getVideoPlayer().pauseVideo();
            currentPosition = getVideoPlayer().isPrepared() ? getVideoPlayer().getVideoCurrentDuration() : 0;
        }
        super.onPause();
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putInt("video_current_position", currentPosition);
        outState.putInt("current_orientation", orientation);
    }

    @Override
    protected void onResume() {
        super.onResume();
        showControls(getVideoPlayer() != null && getVideoPlayer().isPrepared());
        getSupportActionBar().show();
    }

    public void toggleOrientationButtonClick(View v) {
        L.d(TAG, "request orientation change: " + orientation);
        switch (orientation) {
            case ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE:
                orientation = ActivityInfo.SCREEN_ORIENTATION_PORTRAIT;
                setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
                break;
            case ActivityInfo.SCREEN_ORIENTATION_PORTRAIT:
                orientation = ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE;
                setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);
                break;
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        for (int i = 0; i < permissions.length; i++) {
            String permission = permissions[i];
            int result = grantResults[i];
            if (permission.equals(Manifest.permission.WRITE_EXTERNAL_STORAGE) && result == PackageManager.PERMISSION_GRANTED) {
                startDownload();
            }
        }
    }

    @Override
    public void surfaceCreated(SurfaceHolder holder) {
        L.d(TAG, "surfaceCreated()");
        if (getVideoPlayer() != null && getVideoPlayer().isPrepared()) {
            getVideoPlayer().setHolder(holder);
            getVideoPlayer().seekTo(currentPosition);
            getVideoPlayer().pauseVideo();
        } else {
            if (video.isOffline()) {
                prepareVideoForPlay();
            }
//            prepareVideoForPlay();
        }
    }

    @Override
    public void surfaceChanged(SurfaceHolder holder, int format, int width, int height) {
        L.d(TAG, "surfaceChanged()");
    }

    @Override
    public void surfaceDestroyed(SurfaceHolder holder) {
        L.d(TAG, "surfaceDestroyed()");
    }

    /*
    * End of System UI Handles
    * */
}
