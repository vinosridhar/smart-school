package in.sri.smartschool.views.adapter;

import android.Manifest;
import android.net.Uri;
import android.os.Handler;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.Collections;
import java.util.List;

import in.sri.smartschool.R;
import in.sri.smartschool.SmartSchoolApp;
import in.sri.smartschool.models.Video;
import in.sri.smartschool.utils.FileUtils;
import in.sri.smartschool.utils.Listeners;
import in.sri.smartschool.utils.PermissionUtils;
import in.sri.smartschool.utils.VideoUtils;

/**
 * Created by sridhar on 12/12/16.
 */

public class VideosRecyclerViewAdapter extends RecyclerView.Adapter<VideosRecyclerViewAdapter.VideoItemViewHolder> {

    private List<Video> videoList = Collections.emptyList();
    private Listeners.RecyclerViewOnClickListener clickListener;
    public Handler handler = new Handler();

    public VideosRecyclerViewAdapter(List<Video> list, Listeners.RecyclerViewOnClickListener clickListener) {
        this.videoList = list;
        this.clickListener = clickListener;
    }

    @Override
    public VideoItemViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new VideoItemViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.video_item, parent, false), clickListener);
    }

    @Override
    public void onBindViewHolder(VideoItemViewHolder holder, int position) {
        holder.bindData(getItem(position));
    }

    @Override
    public int getItemCount() {
        return videoList.size();
    }

    public Video getItem(int position) {
        return videoList.get(position);
    }

    static class VideoItemViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        private ImageView thumbnail;
        private TextView title;
        private TextView offline;

        private Listeners.RecyclerViewOnClickListener listener;

        public VideoItemViewHolder(View itemView, Listeners.RecyclerViewOnClickListener listener) {
            super(itemView);

            this.listener = listener;

            itemView.setOnClickListener(this);

            thumbnail = (ImageView) itemView.findViewById(R.id.thumbnail);
            title = (TextView) itemView.findViewById(R.id.title);
            offline = (TextView) itemView.findViewById(R.id.offline);
        }

        public void bindData(Video video) {
            title.setText(video.getTitle());
            thumbnailPlaceHolder();
            if (video.isOffline()) {
                //check whether thumbnail exists or not
                if (PermissionUtils.hasPermission(itemView.getContext(), Manifest.permission.READ_EXTERNAL_STORAGE)) {
                    String thumbnailPath = SmartSchoolApp.getThumbnailsDirectory() + video.getFileName() + ".png";
                    if (FileUtils.isFileExists(thumbnailPath)) {
                        thumbnail.setImageURI(Uri.parse(thumbnailPath));
                    } else {
                        if (PermissionUtils.hasPermission(itemView.getContext(), Manifest.permission.WRITE_EXTERNAL_STORAGE)) {
                            if (VideoUtils.createThumbnail(thumbnailPath, video.getLocalFilePath())) {
                                thumbnail.setImageURI(Uri.parse(thumbnailPath));
                            }
                        }
                    }
                }
            }
            offline.setText("Offline - " + (video.isOffline() ? "Yes" : "No"));
        }

        public void thumbnailPlaceHolder() {
            thumbnail.setImageResource(R.drawable.ic_action_maps_local_movies_dark);
        }

        @Override
        public void onClick(View v) {
            if (listener != null) {
                listener.onClick(v, getAdapterPosition());
            }
        }
    }
}
