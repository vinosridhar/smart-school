package in.sri.smartschool.views.activity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;

import java.net.URL;
import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import in.sri.smartschool.R;
import in.sri.smartschool.models.Video;
import in.sri.smartschool.presenters.HomePresenter;
import in.sri.smartschool.utils.L;
import in.sri.smartschool.utils.Listeners;
import in.sri.smartschool.views.HomeView;
import in.sri.smartschool.views.adapter.VideosRecyclerViewAdapter;

/**
 * Created by sridhar on 10/12/16.
 */

public class HomeActivity extends BaseActivity implements HomeView {

    private HomePresenter presenter;
    private VideosRecyclerViewAdapter videosRecyclerViewAdapter;
    private List<Video> videoList = new ArrayList<>();

    @BindView(R.id.toolbar)
    Toolbar toolbar;

    @BindView(R.id.videosRecyclerView)
    RecyclerView videosRecyclerView;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        presenter = new HomePresenter();
        presenter.attachView(this);

        setContentView(R.layout.activity_home);

        ButterKnife.bind(this);

        initUI();

        initComponents();
    }

    private void initUI() {
        videosRecyclerView.setLayoutManager(new LinearLayoutManager(this));
        videosRecyclerView.setHasFixedSize(false);
    }

    private void initComponents() {
        setSupportActionBar(toolbar);

        toolbar.setOnMenuItemClickListener(new Toolbar.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem item) {
                switch (item.getItemId()) {
                    case R.id.menu_about:
                        Intent i = AboutActivity.newIntent(HomeActivity.this);
                        startActivity(i);
                        break;
                }
                return false;
            }
        });

        try {
//            videoList.add(new Video(new URL("https://socialcops.com/video/main.mp4"), "Sample Video", "sample_video.mp4"));
            videoList.add(new Video(new URL("http://www.english-4kids.com/kidsvideos/ABC%20Lesson2.mp4"), "English Alphabets", "english_alphabets.mp4"));
            videoList.add(new Video(new URL("http://www.english-4kids.com/kidsvideos/beginnervideos/olympics/olympics.flv"), "Olympic Games", "olympic_games.flv"));
//            videoList.add(new Video(new URL("http://www.sample-videos.com/video/mp4/720/big_buck_bunny_720p_20mb.mp4"), "Sample Video", "sample_video_1.mp4"));
        } catch (Exception e) {
            L.logException(TAG, e);
        }

        videosRecyclerViewAdapter = new VideosRecyclerViewAdapter(videoList, videoRecyclerViewOnClickListener);
        videosRecyclerView.setAdapter(videosRecyclerViewAdapter);
    }

    @Override
    protected void onResume() {
        super.onResume();
        videosRecyclerViewAdapter.notifyDataSetChanged();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (presenter != null) {
            presenter.detachView();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_home, menu);
        return true;
    }

    @Override
    public Context getContext() {
        return this;
    }

    @Override
    public void back() {
        finish();
    }

    @Override
    public void next() {

    }

    private Listeners.RecyclerViewOnClickListener videoRecyclerViewOnClickListener = new Listeners.RecyclerViewOnClickListener() {
        @Override
        public void onClick(View v, int position) {
            Intent i = VideoActivity.newIntent(HomeActivity.this);
            i.putExtra("video", videosRecyclerViewAdapter.getItem(position));
            startActivity(i);
        }
    };
}
