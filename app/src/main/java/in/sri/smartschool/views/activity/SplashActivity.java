package in.sri.smartschool.views.activity;

import android.Manifest;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import java.util.ArrayList;
import java.util.List;

import in.sri.smartschool.R;
import in.sri.smartschool.utils.L;
import in.sri.smartschool.utils.PermissionUtils;

/**
 * Created by sridhar on 10/12/16.
 */

public class SplashActivity extends BaseActivity {

    public static final int PERMISSIONS_REQUEST_CODE = 100;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_splash);

        if (PermissionUtils.shouldAskPermission()) {
            List<String> permissions = new ArrayList<>();
            if (!PermissionUtils.hasPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE)) {
                permissions.add(Manifest.permission.WRITE_EXTERNAL_STORAGE);
            }

            if (permissions.isEmpty()) {
                start();
                return;
            }

            String[] permissionsToAsk = new String[permissions.size()];
            for (int i = 0; i < permissions.size(); i++) {
                permissionsToAsk[i] = permissions.get(i);
            }

            requestPermissions(permissionsToAsk, PERMISSIONS_REQUEST_CODE);

        } else {
            start();
        }
    }

    public void start() {
        new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    Thread.sleep(1000);
                    Intent i = new Intent(SplashActivity.this, HomeActivity.class);
                    startActivity(i);
                    finish();
                } catch (InterruptedException e) {
                    L.logException(TAG, e);
                }
            }
        }).start();
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        start();
    }
}
