package in.sri.smartschool.views;

import android.content.Context;

/**
 * Created by sridhar on 10/12/16.
 */

public interface BaseView {

    Context getContext();
    void back();
    void next();

}
